import app from 'lib/init'
import 'unfetch/polyfill'
import 'modules/**/*.js'
import 'regenerator-runtime/runtime'
import 'lib/flickity'

document.addEventListener('DOMContentLoaded', () => {
  // This call looks in the DOM for all "data-module"
  // attributes and instantiates relevant modules
  // based on the attributes found.
  app.init()
})
