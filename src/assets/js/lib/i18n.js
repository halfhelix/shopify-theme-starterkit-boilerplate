/*
This module has been installed in order to extract translations from Shopify config files

Usage:

Add the 'path.to_your.translation' on `javascript-lang.liquid`

Import this module on your component and use it aside other properties as data() or mixins:
import { i18n } from 'lib/i18n.js'
...
18n,

Then you will be able to have it on a function as:
const text = this.$t('path.to_your.translation')

Or in yor js file as a scaped string like:
{{ $t('path.to_your.translation') }}

*/
import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

export const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: { en: __GLOBAL__.langMessages }
})
