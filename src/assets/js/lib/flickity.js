import Flickity from 'flickity'

/**
 * @see https://github.com/metafizzy/flickity/issues/534#issuecomment-328859144
 *
 * This update to prototype is a proposed solution by the
 * author of Flickity, to allow carousel cells to have
 * the same height.
 */
Flickity.prototype._createResizeClass = function () {
  this.element.classList.add('flickity-resize')
}

Flickity.createMethods.push('_createResizeClass')

const resize = Flickity.prototype.resize
Flickity.prototype.resize = function () {
  this.element.classList.remove('flickity-resize')
  resize.call(this)
  this.element.classList.add('flickity-resize')
}
