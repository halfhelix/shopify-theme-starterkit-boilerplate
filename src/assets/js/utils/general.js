import select from 'dom-select'

/**
 * A very simple helper function that checks
 * a single breakpoint. Note: this is a way to guess
 * at the device type, it is by no means the best way.
 */
export const isMobile = () => window.innerWidth < 768

/**
 * A very simple helper function that checks
 * a single breakpoint. Note: this value is used
 * in the context of the collection discount page
 */
export const isLgDesktop = () => window.innerWidth >= 1440

/**
 * A very simple helper function that checks a single breakpoint.
 * Note: It uses the same breakpoint than `@media #{$mq-md}`
 */
export const isMediumDesktop = () => window.innerWidth > 1200

/**
 * Checks to see if the device supports touch gestures.
 */
export const isTouch = () => {
  return 'ontouchstart' in window || navigator.maxTouchPoints
}

/**
 * Checks to see if the device supports object-fit styles.
 * This is useful to understand if you need to leverage a
 * shim over object-fit styles.
 */
export const supportsObjectFit = () => {
  let objectFit = false
  for (const prop in document.documentElement.style) {
    if (/object(?:-f|F)it$/.test(prop)) {
      objectFit = true
      break
    }
  }
  return objectFit
}

/**
 * A very simple check to see if the device is an IE device.
 */
export const isIE = () => {
  return navigator.userAgent.toLowerCase().indexOf('msie') > 0
}

/**
 * A very simple check to see if the device is an IE or Edge device.
 */
export const isIEorEdge = () => {
  if (document.documentMode || /Edge/.test(navigator.userAgent)) {
    return true
  } else {
    return false
  }
}

/**
 * A very simple check to see if the device is a Firefox device.
 */
export const isFirefox = () => {
  return navigator.userAgent.toLowerCase().indexOf('firefox') > -1
}

/**
 * A helper function that updates the innerHTML
 * of a DOM element.
 *
 * @param {Node} el The element to modify
 * @param {String} value The value to inject
 */
export const write = (el, value) => {
  el.innerHTML = value
}

/**
 * A helper function to add a class
 * to a DOM element, or an array of DOM elements.
 *
 * @param {Node|Array} item A single DOM element or an Array
 * @param {String} selector  The class to add
 */
export const set = (item, selector) => {
  if (item instanceof Array) {
    for (const i of item) {
      i.classList.add(selector)
    }
  } else {
    item.classList.add(selector)
  }
}

/**
 * A helper function to remove a class
 * to a DOM element, or an array of DOM elements.
 *
 * @param {Node|Array} item A single DOM element or an Array
 * @param {String} selector  The class to remove
 */
export const unset = (item, selector) => {
  if (item instanceof Array) {
    for (const i of item) {
      i.classList.remove(selector)
    }
  } else {
    item.classList.remove(selector)
  }
}

/**
 * A helper function  that checks to
 * see if a DOM element has a class attached
 * to it.
 *
 * @param {Node} item A single DOM element
 * @param {String} selector  The class to check
 */
export const contains = (item, selector) => {
  return item.classList.contains(selector)
}

/**
 * A helper function to toggle the class
 * of a DOM element, or an array of DOM elements.
 *
 * @param {Node|Array} item A single DOM element or an Array
 * @param {String} selector  The class to toggle
 */
export const toggle = (item, selector) => {
  if (item instanceof Array) {
    for (const i of item) {
      i.classList.toggle(selector)
    }
  } else {
    item.classList.toggle(selector)
  }
}

/**
 * Get the height of a DOM element
 *
 * @param {Node} el
 */
export const getHeight = (el) => {
  return `${el.offsetHeight}px`
}

/**
 * Get the width of a DOM element
 *
 * @param {Node} el
 */
export const getWidth = (el) => {
  return `${el.offsetWidth}px`
}

/**
 * Checks to see if the current viewport width
 * is over a certain integer.
 *
 * @param {Integer} breakpoint
 */
export const isOver = (breakpoint) => {
  return (window.innerWidth > breakpoint)
}

/**
 * Clones a complex Javascript object. This is a
 * thorough way to ensure that the object is fully
 * cloned with no references kept in place.
 *
 * @param {Object} obj
 */
export const deepClone = obj => {
  return JSON.parse(JSON.stringify(obj))
}

/**
 * Formats a given integer to a standard fraction.
 *
 * @param {Integer} num A price, with cents
 * @param {Integer} fraction
 */
export const formatPrice = (num, fraction = 2) => {
  return '$' + (Number(num) / 100).toLocaleString('en-EN', {
    minimumFractionDigits: fraction
  })
}

/**
 * Removes whitespace within a string and makes all
 * characters lowercase.
 *
 * @param {String} str
 */
export const handleize = str => str.replace(/[ /_]/g, '-').toLowerCase()

/**
 * Decodes a string that has been encoded through the 'url_encode' Shopify filter
 * @param {*} str
 */
export const decode = str => decodeURIComponent(str).replace(/\+/g, ' ')

/**
 * Generates a URL that is suitable for the Shopify
 * image CDN. Adds the current image size suffix.
 *
 * @param {*} src
 * @param {*} size
 */
export const getImageWithSize = (src = '', size = 1000) => {
  return ('' + src).replace(/([.](?:jpe?g|png))/, `_${size}x$1`)
}

/**
 * Used by components like the product card to select
 * the current product image based on the active color.
 * If there is no active color, a fallback image
 * should be returned if it is defined.
 *
 * @param {*} color
 * @param {*} images
 * @param {*} featured
 * @param {*} fallback
 */
export const getProductImage = (name = '', value = '', images, featured = false, fallback = false) => {
  const key = `${name}-${(value || '').replace(/[/ ]/g, '-')}`.toLowerCase()
  const image = (images || []).find(({ src }) => {
    return ~src.indexOf(key) && ~src.indexOf(
      featured ? 'pos-1' : 'pos-2'
    )
  })
  if (!image) {
    return getImageWithSize(fallback, 600)
  }
  return getImageWithSize(image, featured ? 1200 : 600)
}

/**
 * Decode Storefront Product ID, this is used
 * with Storefront API
 *
 * @param {*} src
 * @param {*} size
 */
export const decodeIDproduct = (id) => {
  return decodeURIComponent(escape(window.atob(id))).split('gid://shopify/Product/')[1]
}

/**
 * Encode from numerical to Storefront Product ID, this is used
 * with Storefront API
 *
 * @param {*} src
 * @param {*} size
 */
export const encodeIDproduct = (id) => {
  return window.btoa('gid://shopify/Product/' + id)
}

/**
 * Decode Storefront Product Variant ID, this is used
 * with Storefront API
 *
 * @param {*} src
 * @param {*} size
 */
export const decodeIDproductVariant = (id) => {
  return decodeURIComponent(escape(window.atob(id))).split('gid://shopify/ProductVariant/')[1]
}

/**
 * Builds an srcset string
 *
 * @param {String} url The master image URL
 */
export const generateSrcset = (url) => {
  if (!url) return false

  const mobileURL = getImageWithSize(url, 768)
  const tabletURL = getImageWithSize(url, 1024)
  const desktopURL = getImageWithSize(url, 1440)

  return `${mobileURL} 768w, ${tabletURL} 1024w, ${desktopURL} 1440w`
}

/**
 * In order to add and remove listeners on demand we need this 2 variables:
 */
const body = document.querySelector('body')
let bodyCloseListeners = []

export const addCloseListener = (callback) => {
  // For modals and ajax cart the listener it's the same, one it's enough
  if (bodyCloseListeners.length >= 1) {
    return
  }
  // Adding this function in order to remove it later
  const customCallback = (event) => {
    // Only works when pressing esc
    if (event.which === 27 || event.keyCode === 27) {
      // Removing focus on any input
      window.focus()
      if (document.activeElement) {
        document.activeElement.blur()
      }
      // Excecuting callback
      callback(event)
    }
  }

  bodyCloseListeners.push({
    element: body,
    type: 'keyup',
    callback: customCallback
  })

  body.addEventListener('keyup', customCallback)
}

export const clearAllCloseListeners = () => {
  bodyCloseListeners.forEach(item => {
    try {
      item.element.removeEventListener(item.type, item.callback, false)
    } catch (error) {
      console.warn(error)
    }
  })
  bodyCloseListeners = []
}

/**
 * Focus first input when form it's displayed
 *
 * @param {*} id Form Id to focus
 * @param {*} delay If form is inside one model delay is necesary to focus first input
 */
export const focusFirstInputOfForm = (id = false, delay = 0) => {
  if (id) {
    setTimeout(() => {
      document.querySelector(`#${id} .text-input__wrapper input`).focus()
    }, delay) // Delay
  }
}

/**
 * Generates a slug from a readable value
 *
 * @param {*} value Readable value
 */
export const slug = (value) => value.toLowerCase().replace(' ', '-')

/**
 * Adds and removes the ability to gain focus using the
 * attribute tabindex="0" and tabindex="-1".
 * When an element has a positive tabindex value, it gets focus.
 * @param {*} element Element that you would like to toggle
 * focus ability
 */

export const toggleFocusAbility = (element) => {
  if (element.tabIndex === -1 ||
    (!element.tabIndex &&
      (!element.tagName === 'A' ||
      !element.tagName === 'BUTTON' ||
      !element.tagName === 'INPUT' ||
      !element.tagName === 'TEXTAREA' ||
      !element.tagName === 'SELECT' ||
      !element.tagName === 'DETAILS')
    )
  ) {
    element.tabIndex = 0
  } else if (element.tabIndex === 0 ||
    (element.tabIndex !== -1 &&
      (element.tagName === 'A' ||
      element.tagName === 'BUTTON' ||
      element.tagName === 'INPUT' ||
      element.tagName === 'TEXTAREA' ||
      element.tagName === 'SELECT' ||
      element.tagName === 'DETAILS')
    )
  ) {
    element.tabIndex = -1
  } else {
    return true
  }
}

/**
 * Selects all focusable elements within an element
 * @param {*} element Element that you would like to search in, for
 * focusable elements
 */

export const getAllFocusableEl = (el) => (
  select.all(
    'a, button, input, textarea, select, details, [tabindex]:not([tabindex="-1"])',
    el
  ) || []
)

/**
 * Gets all focusable elements and changes their tabindex to 0 if set to -1 or
 * chanes their tabindex to -1 if they are set to 0 or have no tabindex set
 */

export const changeTabIndexFocusEl = (el) => {
  getAllFocusableEl(el).forEach((el) => {
    toggleFocusAbility(el)
    return true
  })
}

/**
 * Returns true if Tab is pressed
 */

export const isTabPressed = (e) => {
  return e.key === 'Tab' || e.keyCode === 9
}
