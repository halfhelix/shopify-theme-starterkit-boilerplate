import state from './pdp-state'
import getters from './pdp-getters'
import actions from './pdp-actions'
import mutations from './pdp-mutations'

/**
 * Here, we simply aggregate the definitions laid
 * out in each of the state, getter, action and
 * mutation files within this Vuex module.
 */
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
