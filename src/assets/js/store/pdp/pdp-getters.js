export default {
  activeVariant (state, getters, rootState) {
    return rootState.pdp.activeVariant
  }
}
