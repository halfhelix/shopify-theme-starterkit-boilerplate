import { formatPrice } from 'utils/general'

/**
 * @info
 *
 * *The "store/cart" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the site. Here, we have the
 * Vuex store module that sits behind cart functionality.
 *
 * *This file*
 * This file outlines "getters". Getters are methods
 * that perform modifications to the state value prior
 * to returning the value to the consuming component.
 */
export default {
  subtotal (state, getters, rootState) {
    return formatPrice(rootState.global.cart.total_price)
  }
}
