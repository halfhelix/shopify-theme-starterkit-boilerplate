/**
 * @info
 *
 * *The "store/cart" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the site. Here, we have the
 * Vuex store module that sits behind cart functionality.
 *
 * *This file*
 * This file outlines "mutations". Mutations are intended
 * to be as simple as possible. All modifications to the state
 * object should happen inside a mutation function. Each
 * of these functions must be synchronous. There should be nothing
 * that has a time delay, such as an AJAX request or a setTimeout.
 */
export default {
  bootstrap (state) {
    state.isBootstrapped = true
  },
  setLoading (state, isLoading) {
    state.loading = isLoading
  },
  setCart (state, status) {
    state.open = status
  },
  toggleCart (state) {
    state.open = !state.open
  },
  closeCart (state) {
    state.open = false
  }
}
