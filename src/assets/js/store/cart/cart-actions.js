import cart from 'lib/cart'

/**
 * @info
 *
 * *The "store/cart" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the site. Here, we have the
 * Vuex store module that sits behind cart functionality.
 *
 * *This file*
 * This file outlines "actions". Actions are functions that
 * include logic that, when ready, commits a mutation to the
 * application state. Whereas mutations must be synchronous,
 * actions return a promise and can include asynchronous
 * logical sequences.
 */
export default {
  getCart ({ commit, state }, payload) {
    commit('setLoading', true)

    return cart
      .get()
      .then(data => {
        commit('global/refreshCart', data, { root: true })
        commit('setLoading', false)
      })
  },

  updateItemQuantity ({ commit, state }, { quantity, id }) {
    if (!id) {
      return
    }

    commit('setLoading', true)

    return cart
      .update({ quantity, id })
      .then(data => {
        commit('global/refreshCart', data, { root: true })
        commit('setLoading', false)
        commit('setCart', true)
      })
  },

  openCart ({ commit }) {
    commit('setCart', true)
  },

  toggleCart ({ commit }) {
    commit('toggleCart')
  },

  closeCart ({ commit }) {
    commit('closeCart')
  },

  addToCart ({ dispatch, state, rootState }, { id, quantity }) {
    const { items } = rootState.global.cart

    // Adds the requested `quantity` to the current item.quantity
    const item = items.find(i => i.variant_id === id)
    const totalQuantity = item
      ? item.quantity + quantity
      : quantity

    return dispatch('updateItemQuantity', { id, quantity: totalQuantity })
  }
}
