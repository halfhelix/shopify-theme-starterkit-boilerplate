import state from './cart-state'
import getters from './cart-getters'
import actions from './cart-actions'
import mutations from './cart-mutations'

/**
 * Here, we simply aggregate the definitions laid
 * out in each of the state, getter, action and
 * mutation files within this Vuex module.
 */
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
