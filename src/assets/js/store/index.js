import Vue from 'vue'
import Vuex from 'vuex'

import global from 'store/global'
import cart from 'store/cart'
import product from 'store/product'
import pdp from 'store/pdp'

Vue.use(Vuex)

/**
 * Here, we take all of the independent
 * Vuex modules laid out in the store
 * subdirectories and create a single Vuex
 * store that includes all of them.
 */
export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {},
  modules: {
    global,
    cart,
    product,
    pdp
    // As you build out other state-backed areas
    // of the site, add new Vuex modules here. If you have
    // a complicated PLP or PDP collection of components,
    // you may want to add a Vuex module for it!
  }
})
