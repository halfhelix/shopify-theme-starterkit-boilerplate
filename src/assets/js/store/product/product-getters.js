/**
 * @info
 *
 * *The "store/product" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the PDP.

 * *This file*
 * This file outlines "getters". Getters are methods
 * that perform modifications to the state value prior
 * to returning the value to the consuming component.
 */

export default {
  available (state, getters) {
    return state.variant.available
  }
}
