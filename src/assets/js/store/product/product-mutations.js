/**
 * @info
 *
 * *The "store/product" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the site. Here, we have the
 * Vuex store module that sits behind product functionality.
 *
 * *This file*
 * This file outlines "mutations". Mutations are intended
 * to be as simple as possible. All modifications to the state
 * object should happen inside a mutation function. Each
 * of these functions must be synchronous. There should be nothing
 * that has a time delay, such as an AJAX request or a setTimeout.
 */
export default {
  setLoading (state, isLoading) {
    state.loading = isLoading
  },
  setNewProduct (state, newProduct) {
    const newMetafields = {
      information: {}
      /* Add other metafields here, e.g.
        materials: {},
        washability: {} */
    }
    if (newProduct.metafields) {
      for (const [key, value] of Object.entries(newMetafields)) {
        newProduct.metafields.edges.forEach(element => {
          if (key === element.node.namespace) {
            newMetafields[element.node.namespace][element.node.key] = element.node.value
          }
        })
      }
    }
    state.product = newProduct
    state.metafields = newMetafields
  },
  setVariant (state, newVariant) {
    state.variant.id = newVariant
  },
  setPrice (state, newPrice) {
    state.variant.price = newPrice
  },
  setAvailable (state, newAvailable) {
    state.variant.available = newAvailable
  },
  setOption (state, newOption) {
    state.variant.option = newOption
  }
}
