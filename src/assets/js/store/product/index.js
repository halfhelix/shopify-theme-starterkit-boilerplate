import state from './product-state'
import getters from './product-getters'
import actions from './product-actions'
import mutations from './product-mutations'

/**
 * Here, we simply aggregate the definitions laid
 * out in each of the state, getter, action and
 * mutation files within this Vuex module.
 */
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
