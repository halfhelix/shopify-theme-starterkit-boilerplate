
export default {
  currentModal (state, getters, rootState) {
    return rootState.global.modal.current
  }
}
