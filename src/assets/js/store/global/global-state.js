/**
 * @info
 *
 * *The "store/global" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the site. Here, we have the
 * Vuex store module that sits behind global functionality.
 *
 * *This file*
 * This file outlines "state". This is the starting snapshot
 * of the modules underlying state. As the user interacts with
 * the components tied into this Vuex module, this state is modified
 * away from these default values.
 */
export default {
  cart: __GLOBAL__.cart,
  modal: { current: '' }
}
