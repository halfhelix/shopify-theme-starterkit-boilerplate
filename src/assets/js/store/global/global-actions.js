import {
  focusFirstInputOfForm,
  addCloseListener,
  clearAllCloseListeners
} from 'utils/general'
import { clearAllBodyScrollLocks } from 'body-scroll-lock'

/**
 * @info
 *
 * *The "store/global" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the site. Here, we have the
 * Vuex store module that sits behind cart functionality.
 *
 * *This file*
 * This file outlines "actions". Actions are functions that
 * include logic that, when ready, commits a mutation to the
 * application state. Whereas mutations must be synchronous,
 * actions return a promise and can include asynchronous
 * logical sequences.
 */
let openedModals = []
const closeModalActions = ({ commit, state }) => {
  openedModals.pop()
  if (openedModals.length === 0) {
    // Reset variables
    openedModals = []
    // Clear listeners
    clearAllCloseListeners()
    clearAllBodyScrollLocks()
    commit('openModal', '')
  } else {
    // Saving and removing the last item because it will be added again
    const key = openedModals[openedModals.length - 1]
    openedModals.pop()
    // Reopening modal
    openModalAction({ commit, state }, key)
  }
}
const openModalAction = ({ commit, state }, payload) => {
  openedModals.push(payload)
  addCloseListener(() => {
    closeModalActions({ commit, state })
  })
  commit('openModal', payload)
}

export default {
  openLoginFormModal ({ commit, state }, payload) {
    commit('setForm', 'login')
    openedModals = []
    openModalAction({ commit, state }, 'accountForms')
    focusFirstInputOfForm(payload.form, 100)
  },
  setForm ({ commit, state }, payload) {
    commit('setForm', payload.type)
    focusFirstInputOfForm(payload.form, 100)
  },
  appendModal ({ commit, state }, payload) {
    openModalAction({ commit, state }, payload)
  },
  openModal ({ commit, state }, payload) {
    openedModals = []
    openModalAction({ commit, state }, payload)
  },
  closeModal ({ commit, state }) {
    closeModalActions({ commit, state })
  },
  checkresolution ({ commit, state }) {
    commit('refreshResolution')
  }
}
