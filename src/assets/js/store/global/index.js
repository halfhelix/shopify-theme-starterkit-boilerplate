import state from './global-state'
import getters from './global-getters'
import actions from './global-actions'
import mutations from './global-mutations'

/**
 * Here, we simply aggregate the definitions laid
 * out in each of the state, getter, action and
 * mutation files within this Vuex module.
 */
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
