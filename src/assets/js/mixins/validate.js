import { i18n } from 'lib/i18n.js'

/**
 * This is a simple Vue.js mixin that checks for
 * a validate method on child components. If the method
 * exists it runs the method and ensures that the form
 * does not submit if the method returns a falsy value.
 */
export default {
  data () {
    return {
      validatableFields: [],
      totalItemsCount: 0,
      validItemsCount: 0
    }
  },
  i18n,
  mounted () {
    this.initForm()
  },
  computed: {
    submitBtnClass () {
      if (this.validItemsCount === this.totalItemsCount) {
        return 'btn--primary'
      } else {
        return 'btn--outlined'
      }
    },
    submitBtnDisabled () {
      if (this.validItemsCount === this.totalItemsCount) {
        return false
      } else {
        return true
      }
    }
  },
  methods: {
    initForm () {
      this.validatableFields = this.$children
        .filter(c => {
          return (typeof c.validateInput !== 'undefined')
        })
      this.totalItemsCount = this.validatableFields.length
      this.validItemsCount = this.getValidItemsCount()
    },
    getValidItemsCount () {
      return this.validatableFields
        .map(c => {
          const valid = c.validateInput()
          if (typeof valid === 'object') return valid.valid
          return valid
        })
        .filter(c => c === true)
        .length
    },
    validateInput () {
      if (this.validateMethod) {
        const validation = this.validateMethod({
          value: this.inputValue,
          name: this.name
        })
        if (typeof validation === 'boolean') {
          this.isValid = validation
        } else {
          this.isValid = validation.valid
          this.errorMessage = validation.message
        }
      } else if (this.required) {
        if (!this.inputValue) {
          this.isValid = false
        } else {
          this.isValid = true
        }
      } else {
        this.isValid = true
      }
      return this.isValid
    },
    formData (target = null) {
      const postData = {}
      if (!target) {
        target = this
      }
      if (target) {
        this
          .$children
          .map(c => {
            if (c.name && c.inputValue) {
              postData[c.name] = c.inputValue
            }
            return c
          })
      }
      return postData
    },
    validateAndSubmitForm (e) {
      const isValid = this.$children
        .filter(c => {
          return (typeof c.validateInput !== 'undefined')
        })
        .map(c => {
          const valid = c.validateInput()
          if (typeof valid === 'object') return valid.valid
          return valid
        })
        .every(res => res)
      if (!isValid) {
        e.preventDefault()
      }
    },
    validateForm (e) {
      const isValid = this.$children
        .filter(c => (typeof c.validateInput !== 'undefined'))
        .map(c => {
          const valid = c.validateInput()
          if (typeof valid === 'object') return valid.valid
          return valid
        })
        .every(res => res)
      if (!isValid && e) {
        e.preventDefault()
      }
      return isValid
    },
    valueChange () {
      this.validItemsCount = this.getValidItemsCount()
    },
    validateEmail (input) {
      const result = {
        valid: false,
        message: ''
      }
      const mailformat = /^\w+([+.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
      result.message = this.$t('general.form_errors.required')
      if (input.value) {
        result.message = this.$t('general.form_errors.no_valid_email')
        if (input.value.match(mailformat)) {
          result.valid = true
          result.message = ''
        }
      }
      return result
    },
    validatePassword (input) {
      const result = {
        valid: false,
        message: ''
      }
      result.message = this.$t('general.form_errors.required')
      if (input.value) {
        result.message = this.$t('general.form_errors.password_length')
        if (input.value.length >= 5) {
          result.valid = true
          result.message = ''
        }
      }
      return result
    }
  }
}
