/**
 * This mixin is for accessibility
 */
import {
  getAllFocusableEl,
  toggleFocusAbility,
  isTabPressed
} from 'utils/general'

export default {
  watch: {
    /**
     * There must be a property "isActive" on
     * the component to dictate active state
     *
     * @param {Boolean} value
     */
    isActive (value) {
      this.$nextTick(() => {
        if (value) {
          this.hideOutsideModal()
          this.setInitialFocusStates()
          this.focusOnTitle()
        } else {
          // @todo move back to previous focus element
          // when the user opts out of the modal experience.
          this.removeHiddenOutside()
        }
      })
    }
  },
  computed: {
    focusable () {
      return getAllFocusableEl(this.$el)
    }
  },
  methods: {
    setInitialFocusStates () {
      getAllFocusableEl(this.$el).forEach((el) => {
        if (el.tabIndex === -1) {
          toggleFocusAbility(el)
        }
        return true
      })
    },
    A11yRespondToKeyDown (e) {
      if (!this.isActive) {
        return true
      }

      if (e.key === 'Escape') {
        this.closeCart()
      }

      this.trapFocusAction(e)
    },
    trapFocusAction (e) {
      if (!isTabPressed(e)) {
        return true
      }

      const first = this.focusable[0]
      const last = this.focusable[this.focusable.length - 1]

      if (first !== last) {
        if (e.shiftKey) /* shift + tab */ {
          if (document.activeElement === firstFocusableEl) {
            e.preventDefault()
            last.focus()
          }
        } else /* tab */ {
          if (document.activeElement === last) {
            e.preventDefault()
            first.focus()
          }
        }
      }
    },
    focusOnTitle () {
      (this.$refs.title || {}).focus()
    },
    hideOutsideModal () {
      this.$el.childNodes[0].setAttribute('aria-modal', 'true')
      this.$el.removeAttribute('aria-hidden')
    },
    removeHiddenOutside () {
      this.$el.childNodes[0].setAttribute('aria-modal', 'false')
      this.$el.setAttribute('aria-hidden', 'true')
    }
  }
}
