import Vue from 'vue'
import { mapActions, mapGetters } from 'vuex'
import { disableBodyScroll } from 'body-scroll-lock'
import accessibility from 'mixins/accessibility'

Vue.component('modal', {
  props: {
    handle: String,
    small: {
      type: Boolean,
      default: false
    },
    contentType: String,
    extraClasses: {
      type: String,
      default: ''
    }
  },
  data () {
    return {
      modalElement: null
    }
  },
  mixins: [accessibility],
  mounted () {
    this.bodyElement = document.body
  },
  computed: {
    ...mapGetters('global', [
      'currentModal'
    ]),
    isActive () {
      return this.currentModal === this.handle
    },
    modalContentClasses () {
      let classList = ['modal__inner']
      if (this.contentType) {
        classList = classList.concat(this.contentType.split(' '))
      }
      return classList
    },
    modalClasses () {
      if (this.$scopedSlots.footer) {
        return 'modal__with-footer'
      }
      return ' '
    }
  },
  methods: {
    ...mapActions('global', [
      'setForm',
      'openModal',
      'closeModal'
    ]),
    toggleBackgroundLock () {
      if (this.isActive) {
        disableBodyScroll(this.$refs.container)
      }
    }
  },
  watch: {
    currentModal () {
      this.$nextTick(() => {
        this.toggleBackgroundLock()
      })
    }
  },
  template: '#modal-template'
})
