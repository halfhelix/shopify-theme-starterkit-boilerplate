import objectFitImages from 'object-fit-images'
import Layzr from 'layzr.js'
import { set } from 'utils/general'
import select from 'dom-select'

/**
 * Layzr is a library that provides lazy loading
 * support. This file tells Layzr to review the DOM
 * and look for any modules that should be lazy loaded.
 * This module also provides a fallback for object-fit
 * declarations.
 */
const instance = Layzr({
  normal: 'data-normal',
  retina: 'data-retina',
  srcset: 'data-srcset',
  threshold: 0
})

instance
  .on('src:before', image => {
    const onLoad = function () {
      const container = image.parentNode || false
      const sources = select.all('source', container) || false

      if (sources) {
        for (const source of sources) {
          if (source.hasAttribute('data-srcset')) {
            source.setAttribute('srcset', source.getAttribute('data-srcset'))
            source.removeAttribute('data-srcset')
          }
        }
      }

      if (container) {
        set(container, 'is-loaded')
      }
      objectFitImages()
    }

    /* For some reason, onLoad events are not firing on picture elements in Safari. */
    const fallbackEvent = setTimeout(onLoad, 500)

    image.onload = () => {
      onLoad()
      clearTimeout(fallbackEvent)
    }
  })

export default (el) => {
  instance
    .update()
    .check()
    .handlers(true)
}
