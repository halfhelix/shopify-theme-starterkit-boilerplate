import Vue from 'vue'
import { supportsObjectFit, getImageWithSize } from 'utils/general'
import obzerv from 'obzerv'

/**
 * A Vue.js component to render images within
 * a Vue.js application.
 *
 * @param {String} src The default image src
 * @param {String} srcset An option srcset value
 * @param {String} size Determines the image size to retrive from the server
 * @param {String} alt Alt text to represent the image
 * @param {String} css CSS classes to add to the parent
 * @param {String} fit "cover" or "contain"
 * @param {String} pos "center", "top" or "bottom"
 * @param {Boolean} lazy Should the image load in lazily
 * @param {Boolean} transition Should there be a fade in transition?
 * @param {Boolean} transparent Should the image have a transparent background?
 *
 * @example
 * <div
 *   is="vue-image"
 *   :src="imageSrc"
 *   :alt="title"
 *   :size="300"
 *   fit="cover"
 *   css="product-card__image" />
 */
Vue.component('vue-image', {
  props: {
    src: String,
    srcset: String,
    size: Number,
    alt: String,
    css: String,
    fit: String,
    pos: String,
    lazy: {
      type: Boolean,
      default: true
    },
    transition: {
      type: Boolean,
      default: true
    },
    transparent: {
      type: Boolean,
      default: true
    }
  },
  data () {
    return {
      mainImageSrc: '',
      secondaryImgSrc: '',
      objectFitOk: supportsObjectFit(),
      isInViewport: false,
      states: {
        isLoaded: false
      }
    }
  },
  mounted () {
    if (this.lazy) {
      this.observer = obzerv.create({
        callback: this.lazyLoad
      })
      this.observer.track(this.$refs.wrapper)
    } else {
      this.prepareNewImage()
      this.isInViewport = true
    }
  },
  computed: {
    resized () {
      if (!this.size) return this.src
      return getImageWithSize(this.src, this.size)
    },
    imgLoaded () {
      return this.states.isLoaded ? 'is-loaded' : ''
    },
    imgFit () {
      return (this.fit) ? `fit-${this.fit}` : 'fit-cover'
    },
    imgPos () {
      return (this.pos) ? `position-${this.pos}` : 'position-center'
    },
    backgroundImage () {
      const sources =
      (this.srcset || this.src)
        .split(/\s?,\s?/)
        .reduce((o, val) => {
          const brk = val.split(/\s|w$/)
          o[brk[1] || 0] = brk[0]
          return o
        }, {})

      return sources[Math.max(...Object.keys(sources))]
    }
  },
  methods: {
    prepareNewImage () {
      const img = new Image()
      img.onload = () => {
        this.states.isLoaded = true
      }
      img.src = this.resized
      if (this.srcset) {
        img.srcset = this.srcset
      }
    },
    lazyLoad (node, inview, untrack) {
      if (!inview || this.isInViewport) {
        return false
      }
      this.isInViewport = true
      this.prepareNewImage()
      untrack()
    }
  },
  template: `
    <div
      class="img"
      :class="[css, imgFit, imgLoaded, imgPos, {'no-transition': !transition}, {'transparent': transparent}]"
      ref="wrapper">
      <transition name="fade" mode="in-out">
        <img
          v-if="objectFitOk && isInViewport"
          class="img__el"
          :src="resized"
          :alt="alt"
          :title="alt"
          :data-fit="fit"
          :key="src" />
        <div
          v-if="!objectFitOk && isInViewport"
          class="img__el img__el--ie"
          :title="alt"
          :data-fit="fit"
          :style="{backgroundImage: 'url(' + backgroundImage + ')'}"></div>
      </transition>
    </div>
  `
})
