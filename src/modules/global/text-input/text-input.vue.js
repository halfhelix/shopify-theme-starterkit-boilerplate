import Vue from 'vue'

Vue.component('text-input', {
  props: {
    name: String,
    label: String,
    type: {
      type: String,
      default: 'text'
    },
    modifier: {
      type: String,
      default: 'default'
    },
    autocorrect: Boolean,
    autocapitalize: Boolean,
    initialValueGetter: Function,
    placeholder: String,
    error: String,
    required: Boolean,
    validateMethod: Function
  },
  data () {
    const data = {
      isValid: true,
      isActive: false,
      showErrorMessage: false,
      dirty: false
    }

    if (this.initialValueGetter && this.initialValueGetter.bind !== 'undefined') {
      data.inputValue = this.initialValueGetter(this.name)
    } else {
      data.inputValue = ''
    }

    return data
  },
  watch: {
    inputValue () {
      if (!this.dirty) this.dirty = true

      this.validateInput()

      this.$emit('change', {
        value: this.inputValue,
        name: this.name
      })
    }
  },
  mounted () {
    this.$emit('change', {
      value: this.inputValue,
      name: this.name
    })
    if (this.inputValue) this.setActive()
  },
  methods: {
    validateInput () {
      if (this.validateMethod) {
        const response = this.validateMethod({
          value: this.inputValue,
          name: this.name
        })
        if (typeof response === 'boolean') {
          this.isValid = { valid: response, message: '' }
        } else {
          this.isValid = response
        }
      } else if (this.required && !this.inputValue) {
        this.isValid = { valid: false, message: 'Field is required' }
      } else {
        this.isValid = { valid: true, message: '' }
      }
      return this.isValid
    },
    setActive () {
      this.isActive = true
    },
    unsetActive () {
      if (!this.inputValue) this.isActive = false
    },
    refresh () {
      setTimeout(() => {
        this.inputValue = this.$refs.input.value
        if (this.inputValue) this.setActive()
      }, 0)
    }
  },
  template: `
  <div class="text-input" :class="['text-input--'+modifier, (!isValid)?'form-row__error': '' ]">
    <div class="text-input__wrapper">
      <input
        ref="input"
        class="text-input__el p2"
        :type="type"
        :name="name"
        :autocorrect="autocorrect"
        :autocapitalize="autocapitalize"
        :placeholder="(label ? label : placeholder)"
        v-model="inputValue"
        v-on:focus="setActive"
        v-on:blur="unsetActive" />
    </div>
    <transition name="fade">
      <p v-if="dirty && !isValid.valid" class="form-row__error__text-error red">{{error}}</p>
    </transition>
  </div>`
})
