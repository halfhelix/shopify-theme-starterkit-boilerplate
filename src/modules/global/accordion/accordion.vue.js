import Vue from 'vue'
import { i18n } from 'lib/i18n.js'

let i = 0

Vue.component('accordion', {
  props: ['callback', 'mod_id', 'mod_class', 'mod_title_class', 'mod_title', 'mod_content_class', 'mod_content', 'single_active'],
  i18n,
  data () {
    return {
      isActive: false,
      i: i++
    }
  },
  computed: {
    isOpen () {
      if (!this.isActive) {
        return 'hidden'
      }
      return 'visible'
    },
    height () {
      if (!this.isActive) {
        return 0
      }
      return this.$refs['content'].getBoundingClientRect().height + 'px'
    },
    openText () {
      return `${this.$t('general.actions.open')} ${this.mod_title}`
    },
    closeText () {
      return `${this.$t('general.actions.close')} ${this.mod_title}`
    }
  },
  methods: {
    toggle () {
      this.isActive = !this.isActive

      this.$emit('toggle', this)
    }
  },
  template: `
  <div
    class="accordion"
    :id="[mod_id]"
    :class="[mod_class, {'is-active': isActive}]">
    <div>
      <h2>
        <button
          class="accordion__title block w1 align-l relative indigo"
          :class="mod_title_class"
          @click="toggle"
          :aria-expanded="(isActive) ? 'true' : 'false'"
          :aria-controls="'accordion_' + i"
        >
          {{ mod_title }}
          <div class="accordion__title__icon">
          </div>
        </button>
      </h2>
      <div class="accordion__body" :style="{maxHeight: height, visibility: isOpen}" :id="'accordion_' + i">
        <div class="accordion__body__content" :class="mod_content_class" ref="content">
          <div v-if="mod_content" v-html="mod_content"></div>
          <slot v-else></slot>
        </div>
      </div>
    </div>
  </div>
  `
})
