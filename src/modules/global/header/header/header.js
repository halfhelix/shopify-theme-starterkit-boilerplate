import Sticky from 'sticky-js'
import on from 'dom-event'
import select from 'dom-select'
import { set, unset, contains } from 'utils/general'
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock'

export default el => {
  const navigation = select('.js-header-navigation.js-is-mobile', el)
  const hamburger = select('.js-hamburger', el)
  const promo = select('.js-promo-bar', el)

  enableStickyHeader()
  enableHamburgerToggle(el, hamburger, navigation, promo)
}

function enableStickyHeader () {
  const sticky = new Sticky('.js-header-inner', {
    wrap: true
  })
}

function enableHamburgerToggle (el, hamburger, navigation, promo) {
  on(hamburger, 'click', e => toggleMobileMenu(e, hamburger, navigation, header, promo))
}

/**
 * Enables toggle on/off functionality when
 * the hamburger icon is clicked, activating
 * and deactivating the mobile menu.
 *
 * @param {Object} e the click event
 * @param {DOM Node} hamburger
 * @param {DOM Node} navigation
 * @param {DOM Node} promo
 */
function toggleMobileMenu (e, hamburger, navigation, header, promo) {
  const isActive = contains(hamburger, 'is-active')

  if (isActive) {
    unset(hamburger, 'is-active')
    unset(navigation, 'is-visible')
    unset(header, 'is-active')
    enableBodyScroll(navigation)
  } else {
    set(hamburger, 'is-active')
    set(navigation, 'is-visible')
    set(header, 'is-active')
    disableBodyScroll(navigation)
    setMobileHeaderHeight(header, navigation, promo)
  }
}

/**
 * The mobile menu slides out underneath the header.
 * Since the header is of flexible height, we take it
 * into account when setting height and top offset.
 *
 * @param {DOM Node} hamburger
 * @param {DOM Node} navigation
 * @param {DOM Node} promo
 */
function setMobileHeaderHeight (header, navigation, promo) {
  const {
    height: headerHeight = 0,
    top: headerOffset = 0
  } = header.getBoundingClientRect()
  const {
    height: promoHeight = 0
  } = promo.getBoundingClientRect()
  const navigationOffset = headerHeight + Math.max(headerOffset, -promoHeight)
  navigation.style.height = (
    `${window.innerHeight - navigationOffset}px`
  )
  navigation.style.top = (
    `${navigationOffset}px`
  )
}
