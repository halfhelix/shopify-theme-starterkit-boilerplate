import Vue from 'vue'
import { mapState, mapActions } from 'vuex'
import { formatPrice } from 'utils/general'
import debounce from 'lodash/debounce'
import { i18n } from 'lib/i18n.js'

Vue.component('header-cart-item', resolve => {
  Promise.all([
    enqueueModule('quantity-selector.vue'),
    enqueueModule('vue-image.vue')
  ]).then(() => {
    resolve({
      i18n,
      name: 'header-cart-item',
      props: ['single', 'cartPage', 'maxQty'],
      data () {
        return {
          model: false,
          showQTY: true,
          insoleVariant: false
        }
      },
      watch: {
        single (_new, _old) {
          if (_old.quantity !== _new.quantity) {
            this.showQTY = false
            setTimeout(() => { this.showQTY = true }, 100)
          }
        }
      },
      computed: {
        ...mapState('global', [
          'cart',
          'isMobile'
        ]),
        ...mapState('cart', [
          'cart_insoles',
          'loading'
        ]),
        option () {
          return `${this.single.options_with_values[0].name}: ${this.single.options_with_values[0].value}`
        },
        maximumText () {
          return this.$t('cart.label.maximum')
        }
      },
      methods: {
        ...mapActions('cart', [
          'updateItemQuantity',
          'addToCart'
        ]),
        updateQuantity: debounce(function (quantity) {
          this.updateItemQuantity({
            quantity,
            id: this.single.id
          })
        }, 450),
        remove () {
          this.updateItemQuantity({
            quantity: 0,
            id: this.single.id
          })
        },
        priceFormat (price) {
          return formatPrice(price)
        }
      },
      template: `
      <div class="w1 relative f jcb">
        <div class="header-cart__element__image relative">
          <div
            is="vue-image"
            :src="single.featured_image.url"
            :size="10"
            :alt="single.product_title"
            spinner-size="sm"
            css="fit-cover"
          ></div>
        </div>
        <div class="header-cart__element__data">
          <div
            class="header-cart__element__title p3 uppercase black"
            v-text="single.product_title"
          ></div>
          <div
            class="header-cart__element__price p3"
            v-text="priceFormat(single.price)"
          ></div>
          <div
            class="header-cart__element__option p3 dark-grey"
            v-text="single.options_with_values[0].value"
          ></div>
          <div class="relative cart-page-element__qty" :class="{'cart-page-element__qty--loading': (loading === single.id)}">
            <div
              is="quantity-selector"
              v-if="showQTY"
              @change="updateQuantity"
              :value="single.quantity"
              :min="0"
              :max="maxQty"
              :small="true"
              ></div>
              <div v-else class="quantity-selector--loading"></div>
            <p v-if="single.quantity >= maxQty" v-text="maximumText" class="header-cart__element__maximun p2 red"></p>
          </div>
        </div>
      </div>
      `
    })
  })
})
