import on from 'dom-event'
import select from 'dom-select'
import { set, unset, contains } from 'utils/general'

export default el => {
  const links = select.all('.js-link.has-subnav', el)
  const isMobileVersion = contains(el, 'js-is-mobile')

  if (!isMobileVersion) {
    return
  }

  links.forEach(link => {
    on(link, 'click', e => showSubMenu(e, el))
  })
}

/**
 * Conditionally show the submenus via
 * accordion expand/contract logic. Here,
 * we explicitly set the height to ensure a
 * smooth and well timed animation.
 *
 * @param {Object} e native event object
 * @param {DOM Node} el module root DOM Node
 */
function showSubMenu (e, el) {
  if (contains(e.target, 'js-sub-link')) {
    return true
  }

  e.preventDefault()

  const isActive = contains(e.target, 'is-active')
  const subnav = select('.js-subnav', e.target.parentNode)

  if (isActive) {
    subnav.style.height = null
    unset(e.target, 'is-active')
  } else {
    subnav.style.height = (
      `${subnav.children[0].clientHeight}px`
    )
    set(e.target, 'is-active')
  }
}
