import Vue from 'vue'
import store from 'store'
import { mapState, mapActions } from 'vuex'
import { formatPrice } from 'utils/general'
import accessibility from 'mixins/accessibility'

export default el => new Vue({
  el,
  store,
  mixins: [accessibility],
  computed: {
    ...mapState('global', [
      'cart'
    ]),
    ...mapState('cart', [
      'open',
      'loading'
    ]),
    cartStatus () {
      return this.open
    },
    cartItems () {
      return (this.cart.items || [])
    },
    cartTotal () {
      return formatPrice(this.cart.total_price)
    },
    isActive () {
      return this.open
    }
  },
  methods: {
    ...mapActions('cart', [
      'toggleCart',
      'closeCart'
    ]),
    toogle () {
      this.toggleCart()
    },
    close () {
      this.closeCart()
    },
    priceFormat (price) {
      return formatPrice(price)
    },
    redirectToCheckout () {
      document.location = '/checkout'
    }
  }
})
