import Vue from 'vue'
import store from 'store'
import select from 'dom-select'
import { mapState, mapActions } from 'vuex'

export default el => new Vue({
  el,
  store,
  computed: {
    ...mapState('global', [
      'cart'
    ]),
    itemCount () {
      return (this.cart.items || [])
        .reduce((count, item) => {
          count += Number(item.quantity)
          return count
        }, 0)
    },
    itemCountText () {
      return `Your bag - ${this.itemCount} items`
    }
  },
  methods: {
    ...mapActions('cart', [
      'toggleCart'
    ]),
    toggleCart () {
      this.toggleCart()
      setTimeout(function () {
        if (select('.js-focus-return')) {
          select('.js-focus-return').focus()
        }
      }, 0)
    }
  }
})
