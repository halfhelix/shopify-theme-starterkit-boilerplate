# Shopify Theme Starterkit

This theme is intended to provide a structure and a series of modules to guide the development of Shopify themes at Half Helix. It is complimented by a development, build and deployment CLI package called [@halfhelix/kit](https://www.npmjs.com/package/@halfhelix/kit).

> This Starterkit is in it's initial stages and is not considered stable. The modules are in active development and are likely to have issues present.

### Getting started

We have set up a dedicated development store for playing around with this starterkit: `https://starterkit-playground.myshopify.com`. The login for this instance is available under our Shopify Partner's account. Feel free to log in and get setup via the steps below.

**Step One**

Make sure that you have [@halfhelix/kit](https://www.npmjs.com/package/@halfhelix/kit) installed on your computer globally. This can be done by running `npm i -g @halfhelix/kit`.

**Step Two**

Run `npm i` to install the theme's local dependencies

**Step Three**

Copy the `.env-example` to create a new `.env` file in the root of the repository. In order to do this, you'll need to provide the theme ID (you may need to create a theme in the Shopify instance), the Shopify instance URL and the private app's password.

**Step Four**

Run `kit watch` to start development.

### Deploying

To deploy the theme, run `kit deploy --env production` or `npm run deploy`. This will compile and upload the theme to Shopify.

### Build

If you just want to build the theme, run `kit build --env production` or `npm run build`. This will compile the theme into the configured build directory (this defaults to `./dist`).

### Roadmap

1. Stabilize current modules
1. Test and ensure CI tasks are working as expected
3. Add new modules (exact modules TBD)
4. Validate the Starterkit through a series of new projects

### Feedback

This theme is new and introduces a number of new paradigms. We encourage you to play around and feedback to the team with ideas and improvements.

### Bugs & Missing Information

This package is currently unstable and in it's initial stages. Expect bugs and missing information. We encourage you to submit tickets and let us know the issues you are experiencing. Do this via Slack or even through Gitlab Issues.
